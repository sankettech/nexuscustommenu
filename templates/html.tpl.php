<!DOCTYPE html>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>
<head>
<?php print $head; ?>

<script>

// on hover background image of the server banner changes

document.getElementById("#menu-362-1 li").onmouseover = changeServerImage;

function changeServerImage() {
    document.getElementById("banner-server").style.backgroundImage = "url('aws.png')";

}

// on hover background image of the aircraft banner changes

document.getElementById("#menu-349-1 li").onmouseover = changeAircraftImage;

function changeAircraftImage() {
    document.getElementById("banner-aircraft").style.backgroundImage = "url('ac.png')";

}

</script>

<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<?php print $scripts; ?>
<!--[if lt IE 9]><script src="<?php print base_path() . drupal_get_path('theme', 'nexus') . '/js/html5.js'; ?>"></script><![endif]-->

</head>
<body class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>